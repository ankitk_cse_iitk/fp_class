#!/usr/bin/env runhaskell

In this program we use the applicative instance of Haskell list to an
important real world problem: generate topics for sales talk. This is version 0.1
of the program and hence only generates the topic for discusion

> module ManagerSpeak where

> import Control.Applicative


A successful sales talk is nothing but a list of jargons thrown at
such a high speed at the client, a socially acceptable way of saying
victim, that he has very little time to think. The variable below
gives you a list of jargons that one can use during a sales talk.  A
jargon is essentially a combination of an adjective


> jargon :: [String]
> jargon = combine <$> adjectives <*> subjects
>   where combine x y = unwords [x, y]


Our vocabulary is rather limited here. We can grow in future versions of the
software. Here is the list of adjectives

> adjectives :: [String]
> adjectives =  [ "innovative", "scalable", "enterprise grade", "intelligent" ]

and  subjects.

> subjects   :: [String]
> subjects   =  [ "controller", "processor", "storage", "media"]


Finally here is the printed list of all jargons.

> main :: IO ()
> main = putStrLn $ unlines jargon
