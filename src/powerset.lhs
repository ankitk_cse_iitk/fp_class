#!/usr/bin/env runhaskell

Learn You A Haskell has a cool implementation of the powerset function, as shown below :

>import Control.Monad

>powerset :: [a] -> [[a]]  
>powerset = filterM (\x -> [True, False])

>main = print $ powerset [1,2,3]

λ> powerset [1,2,3]
[[1,2,3],[1,2],[1,3],[1],[2,3],[2],[3],[]]

How does it work ?
filterM is the Monadic version of filter function, which is used to filter elements from a list using a predicate.

filter :: (a -> Bool) -> [a] -> [a]
filterM :: Monad m => (a -> m Bool) -> [a] -> m [a]

So, in this case m is the List Monad. Thus m Bool is the List containing True and False, which can be seen as Non-deterministically either choosing or removing elements from the input list. 
Since filterM is applied to input list, we get all the possible outcomes where each element is either filtered (kept) in the list or not. So, we get the powerset of the input list.
